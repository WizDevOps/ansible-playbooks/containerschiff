# dotfiles

Deploy all the dotfiles (bashrc, tmux.conf et. al). This also is responsible
for installing `oh-my-bash`.

# Requirements

None

# Role Variables

None

# Dependencies

None

# License

GPLv3

# Author Information

Daniel Andrei Minca <dminca@pm.me>
