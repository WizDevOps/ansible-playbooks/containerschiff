# Bootstrap

Common tasks for bootstrapping the workstation

# Requirements

None

# Role Variables

When running **ArchLinux**, the following params are exposed

| Option  | Default  | Description  |
|---|---|---|
| `toggle_polybar`  | `False`  | if `polybar` is installed from AUR, set this to `true` before running the playbook  |
| `i3gaps`  | `False`  | set `i3-gaps` as a `user-session` in `lightdm.conf`   |

# Dependencies

None


# License

GPLv3
