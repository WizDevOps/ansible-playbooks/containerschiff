# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v6.0.0] - 2019-02-23
### Added
- `README` for the `bootstrap` role with exposed params
- [neovim](https://neovim.io) as default `${EDITOR}`
  - configuration file !106
  - installation of a plugin manager (vim-plug) !106
- tags in the `bootstrap` role for running the operations in a separate manner
  - `pkgs` - installs required APT packages on OS
  - `dots` - installs/configures the dotfiles in `${HOME}` dir
  - `bash` - install/configures `oh-my-bash` - because it's the fastest!
- [fzf](https://github.com/junegunn/fzf) configuration in [bashrc](roles/bootstrap/templates/bashrc.j2)
- [gitconfig](roles/bootstrap/templates/gitconfig.j2) is now using credential
  store for cloning everything via HTTPS because it's much faster & safer than
  SSH protocol
### Changed
- variables regarding `bootstrap` role from the root `README` file (they can
  now be found in the [roles/bootstrap/README](roles/bootstrap/README.md)
- APT packages installation for Debian distro - updated to the latest way of
  installing a chunk of packages (compatible with latest Ansible version)
- `git commit -S` aliased to `gcs` - no longer applies PGP signature on commit,
  instead another alias was created to apply signature -> `gcss`
- [bashrc](roles/bootstrap/templates/bashrc.j2) has been cleaned up, refactored
  conditional code
- default `${EDITOR}` to `editor` (which defaults to whatever's been set by
  `update-alternatives`)
- default diff/merge tool in [gitconfig](roles/bootstrap/templates/gitconfig.j2)
  that's now being set to `nvim` diff
### Deprecated
- vim installation/configuration
### Removed
- vim as default `${EDITOR}`
- useless APT packages from the APT package list (no longer bloating the OS
  with that crap anymore)
- Ubuntu apport disable script - this was the stupid Troubleshooting tool in
  Ubuntu that's been finally removed once and for all
- `redshift` package installation and configuration file - no longer requiring
  to install/configure a blue-light filter
- `gpg.conf` no longer being deployed and maintained - a separate task must be
  created for that in order to be done properly (ideally the functionality
  should import the private PGP key and create the truststore et. al.)
- git configuration for work profile - no longer using work on personal
  workstation (work/life balance)
- PGP key from git configuration (no longer signing commits)
- git configuration file for work profile

## [v5.4.0] - 2018-08-13
### Added
- support for `i3-gaps`, `polybar` and `compton` on i3 ( !102 )

### Fixed
- CHANGELOG urls that were pointing to the previous address of this playbook
  before moving it to the `ansible-playbooks` GitLab group
- markdown syntax from CHANGELOG
- `vim-ansible-role` url from [requirements.yml](requirements.yml) file
- VGA driver issue on Notebook environment; the wrong driver was installed and
  was causing some minor malfunctioning, replaced with the driver from Intel as
  suggested by the ArchWiki

### Removed
- `xdotool` package from ArchLinux requirements since it's not handling tiling
  window managers pretty well (i3-wm)

## [v5.3.0] - 2018-06-08
### Added
- support for touchpad toggle ON/OFF via <kbd>FN</kbd>+<kbd>F9</kbd>
- support for keyboard layout switching (EN/RO/RU languages)
- `powertop` utility for optimizing battery save
- installation of `evince` for all Desktop Envrionments (DE)
- `VirtualBox` role for ArchLinux for installing & configuring VirtualBox
- install and configure `urxvt` terminal for `i3wm` DE
- configure X and locale + keyboard layout
- install the `Hack` font
- install `pcmanfm` File Manager for `i3wm`
- hotkey for launching PyCharm IDE
- System Locale configuration in the X configuration file
- enable [clickable URLs][1] on URXVT
- `i3wm` reload handler after applying changes to configuration file
- setup wallpaper and volume icon in tray bar for `i3wm`
- install and configure `oh-my-bash`
- autostart bluetooth applet on laptop on-boot
- install and configure `dpaste` script for pasting code on the web and sharing
  link to community easily for debugging purposes
- add PyCharm generated files to `.gitignore`
- install and configure `flameshot` for Print Screen
- install `bash-completion` pkg for autocomplete in bash
- install and deploy configuration file for `redshift` (blue light filter)
- install RFC script for fetching and reading RFC's on the cli

### Changed
- YAML inventory instead of CFG for simplicity sake
- set VIM as default IDE on bash
- refactor list of dicts in dotfiles configuration task
- enable task profiling when running the Ansible playbook
- disable keyboard layout switching since it's conflicting with Sublime Text 3
- remove `python-pip` from the list of common ArchLinux packages; will install it
  how the [Python community][2] suggested #104
- show the diff in the IDE before committing in git
- disabled `git` plugin from `oh-my-bash`; it was conflicting with our own
  `bash_aliases`
- add another `roles_path` in `ansible.cfg` for successfully finding the roles
- `pacman v5.1` release; install missing package
- refactor Sublime Text installation role

### Fixed
- docker installation issue; the `/etc/docker` folder wasn't present before
  deploying the `daemon.json` configuration file
- installation of `keepassxc` instead of `keepassx2`
- issue with `include_*` with Jinja2 conditional
- `i3wm` DE reload handler
- critical issue with default TTY for current user that was denying access on
  the DE eventhough the password was correct; `/usr/bin/bash` wasn't in `shells(5)`
- `NVIDIA` driver installation for laptop ArchLinux
- optimize the `ansible_managed` Ansible variable from Jinja2 templates
- launching default web browser via `xdg-open`

### Removed
- vim syntax templates
- Ubuntu font family from common ArchLinux packages list
- obsolete dotfiles from the `bootstrap` role
- useless TravisCI configuration
- obsolete code that was used for starting processes on different workspaces
  after boot
- obsolete `bash_aliases` and `zshrc`
- useless zsh configuration tasks

## [v5.2.0] - 2018-03-18
### Added
- tagged window manager reload task for applying just that configuration
  instead of running the whole playbook
- install packages for ArchLinux and RedHat:
  - tcpdump
  - mtr
  - chromium
- install packages on ArchLinux:
  - gimp
  - unzip
  - nmap
  - evince (needed a pdf reader)
- list of packages to be installed on Notebook only
- touchpad toggle (on/off) on notebook only for easily disabling it when mouse
  is pluged in or when typing
- limit text width to 80 characters in the [CHANGELOG](CHANGELOG.md) and
  [README](README.md)

### Changed
- Ansible roles path in [ansible.cfg](ansible.cfg); just clone the role in the
  project dir where all roles are stored so it's easier to version it
- `vim-ansible-role` name changed to its actual repository name for easily
  switching to it in vim and finding it properly

### Fixed
- RPM Fusion repository provisioning for RedHat distribution; missing jinja2
  escape statement (double quotes and surrounded by double braces)
- sound issues on ArchLinux; installed missing packages (`alsa-utils` and
  `pulseaudio-alsa`)

### Deprecated
- disable autostart processes on i3wm system boot; wasn't functioning very well

### Removed
- duplicate text from [CHANGELOG](CHANGELOG.md)
- xautolock package; it wasn't locking screen after inactivity anyway
- `ansible` vim filetype; was screwing up indentation, `yaml` filetype
  indentation works better out of the box and it's native

## [v5.1.0] - 2018-03-04
### Added
- `xautolock` on ArchLinux for auto-locking screen
- useful Jinja2 URL resource in [README](README.md)
- LibreOffice package to `bootstrap` role for installing on Debian
- Powerline font for `vim-airline`
- weekly cron on ArchLinux for updating mirrorlist

### Changed
- Ansible default Python interpreter to Python3
- `packages` rolename to `bootstrap`
- merged into `master` the following docker roles:
  - `archlinux` branch
  - `fedora` branch
  - [WizDevOps.debian-docker-toolbox](https://gitlab.com/WizDevOps/ansible-roles/debian-docker-toolbox)

### Fixed
- typos and URLs in [CHANGELOG](CHANGELOG.md)
- subdirectory creation for project root dir (`~/Repos `)
- Sublime Text installer
- dynamic imports based on `ansible_os_family`

### Deprecated
- Sublime Text role from main playbook

### Removed
- LibreOffice debian role
- `WizDevOps.debian-docker-toolbox` role

## [v5.0.0] - 2018-03-04
### Added
- [CHANGELOG](CHANGELOG.md) for keeping track of changes better
- 3x distros on Vagrantfile for testing playbook: Archlinux, Debian, Fedora
- more templates in the packages role that were missing (ie. i3 config..)
- task to install i3 window manager dependencies ( #91 )

### Changed
- remove bloated information from [README.md](README.md)
- each tasks are separated by distribution
  - Archlinux
  - Fedora
  - Ubuntu
- separate Window Manager tasks:
  - LXDE
  - i3

### Removed
- obsolete variables from packages role for Openbox
- obsolete Openbox presence check
- roles from playbook that aren't integrated yet on master branch

[Unreleased]: https://gitlab.com/WizDevOps/ansible-playbooks/containerschiff/compare/v6.0.0...master
[v6.0.0]: https://gitlab.com/WizDevOps/ansible-playbooks/containerschiff/compare/v5.4.0...v6.0.0
[v5.4.0]: https://gitlab.com/WizDevOps/ansible-playbooks/containerschiff/compare/v5.3.0...v5.4.0
[v5.3.0]: https://gitlab.com/WizDevOps/ansible-playbooks/containerschiff/compare/v5.2.0...v5.3.0
[v5.2.0]: https://gitlab.com/WizDevOps/ansible-playbooks/containerschiff/compare/v5.1.0...v5.2.0
[v5.1.0]: https://gitlab.com/WizDevOps/ansible-playbooks/containerschiff/compare/v5.0.0...v5.1.0
[v5.0.0]: https://gitlab.com/WizDevOps/ansible-playbooks/containerschiff/compare/4.2.0...v5.0.0

[1]: https://wiki.archlinux.org/index.php/Rxvt-unicode#Clickable_URLs
[2]: https://pip.pypa.io/en/stable/installing/
<!--
vim: tw=79:foldmethod=indent
-->
