# Workstation configuration
> Some ansible roles for configuring personal workstation

## Useful resources:
- [Best Practices/Directory Layout][1]
- [Ansible Cookbook - tips & tricks][2]
- [DigitalOcean and AWS on Ansible][3]
- [Nginx, MysSQL, PHP on Ansible][4]
- [vpn, tor, zsh on Ansible][5]
- [mongodb, supervisor, openssl, nodejs, memcached, php5 on Ansible][6]
- [Jinja2 template designer documentation][7]

[1]: http://docs.ansible.com/ansible/playbooks_best_practices.html
[2]: http://ansiblecookbook.com/downloads/ansiblecookbook.en.pdf
[3]: https://github.com/adithyakhamithkar/ansible
[4]: https://github.com/heybigname/ansible/tree/master/tasks
[5]: https://github.com/RaymiiOrg/ansible
[6]: https://github.com/M4nu/ansible
[7]: http://jinja.pocoo.org/docs/dev/templates/
<!--
vim: tw=79
-->
